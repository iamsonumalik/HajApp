package com.app.hajapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

public class MainActivity extends Activity {

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();


        TabHost.TabSpec spec = tabHost.newTabSpec("tab_creation");
        spec.setIndicator("HUJAJ INFO", getResources().getDrawable(android.R.drawable.ic_menu_add));
        spec.setContent(R.id.tablinearLayout);
        tabHost.addTab(spec);

        TabHost.TabSpec spec2 = tabHost.newTabSpec("tab_creation");
        spec2.setIndicator("TAWAF", getResources().getDrawable(android.R.drawable.ic_menu_add));
        spec2.setContent(R.id.tablinearLayout2);
        tabHost.addTab(spec2);

        TabHost.TabSpec spec3 = tabHost.newTabSpec("tab_creation");
        spec3.setIndicator("PERSONAL", getResources().getDrawable(android.R.drawable.ic_menu_add));
        spec3.setContent(R.id.tablinearLayout3);
        tabHost.addTab(spec3);
        tabHost.getTabWidget().getChildTabViewAt(0).setBackgroundDrawable(getResources().getDrawable(R.drawable.tabback));
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                switch (tabHost.getCurrentTab()) {
                    case 0:
                        tabHost.getTabWidget().getChildTabViewAt(0).setBackgroundDrawable(getResources().getDrawable(R.drawable.tabback));
                        tabHost.getTabWidget().getChildTabViewAt(1).setBackgroundDrawable(null);
                        tabHost.getTabWidget().getChildTabViewAt(2).setBackgroundDrawable(null);

                        break;
                    case 1:
                        tabHost.getTabWidget().getChildTabViewAt(1).setBackgroundDrawable(getResources().getDrawable(R.drawable.tabback));
                        tabHost.getTabWidget().getChildTabViewAt(0).setBackgroundDrawable(null);
                        tabHost.getTabWidget().getChildTabViewAt(2).setBackgroundDrawable(null);

                        break;
                    case 2:
                        tabHost.getTabWidget().getChildTabViewAt(2).setBackgroundDrawable(getResources().getDrawable(R.drawable.tabback));
                        tabHost.getTabWidget().getChildTabViewAt(1).setBackgroundDrawable(null);
                        tabHost.getTabWidget().getChildTabViewAt(0).setBackgroundDrawable(null);

                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
